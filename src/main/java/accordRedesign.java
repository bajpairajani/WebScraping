
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.csvreader.CsvWriter;
import utility.Constants;
public class accordRedesign {

		public static void main(String[] args) throws IOException, InterruptedException {
			System.setProperty("webdriver.chrome.driver",Constants.CHROMEDRIVER);
			WebDriver driver = new ChromeDriver();
			
			//CSV file where you want to save the content of the website. It should be blank
			String csvOutputFile = Constants.EXPORTTOCSV;
			//check if file exist
	        boolean isFileExist = new File(csvOutputFile).exists();
	        
			int i =0;
			int f =3640;
			//DecimalFormat twodigits = new DecimalFormat("00");
			for( i=1; i<=10; i++) {
				        
		            try {
		            	driver.get(Constants.URL);
		            	Thread.sleep(10000);
		            	
		            	driver.findElement(By.cssSelector("div.content-block ul li:nth-child("+i+") div.information span a div span")).click();
		            	Thread.sleep(3000);
		                //create a FileWriter constructor to open a file in appending mode
		                CsvWriter testcases = new CsvWriter(new FileWriter(csvOutputFile, true), ',');
		                //write header column if the file did not already exist
		                System.out.println("file exists = "+isFileExist);
		                if(isFileExist && i==1)
		                {
		                	testcases.write("Record for");
		                	testcases.write("Hotel or Entity");
		                    testcases.write("Location");
		                    testcases.write("Job Category");
		                    testcases.write("Job Information");
		                    testcases.write("Extra One");
		                    testcases.write("Extra two");
		                    testcases.write("Extra Three");
		                    testcases.write("Extra Four");
		                 
		                    //end the record
		                    testcases.endRecord();
		                }
		                Thread.sleep(6000);
		                
		                if( driver.findElements(By.className("job-title")).size() > 0)
		                	testcases.write(driver.findElement(By.className("job-title")).getText());
		                else
		                	testcases.write("-");
		                
		                if( driver.findElements(By.className("hotelName")).size() > 0)
		                	testcases.write(driver.findElement(By.className("hotelName")).getText());
		                else
		                	testcases.write("-");
		                
		                if( driver.findElements(By.className("job-location")).size() > 0)
		                	testcases.write(driver.findElement(By.className("job-location")).getText());
		                else
		                	testcases.write("-");
		                
		                if( driver.findElements(By.className("job-category")).size() > 0)
		                	testcases.write(driver.findElement(By.className("job-category")).getText());
		                else
		                	testcases.write("-");
		                Thread.sleep(3000);

		                if( driver.findElements(By.cssSelector("div.jd-info ul:nth-of-type(1)")).size() > 0)
		                	testcases.write(driver.findElement(By.cssSelector("div.jd-info ul:nth-of-type(1)")).getText());
		                else
		                	testcases.write("-");

		                if( driver.findElements(By.cssSelector("div.jd-info ul:nth-of-type(2)")).size() > 0)
		                	testcases.write(driver.findElement(By.cssSelector("div.jd-info ul:nth-of-type(2)")).getText());
		                else
		                	testcases.write("-");
		                if( driver.findElements(By.cssSelector("div.jd-info ul:nth-of-type(3)")).size() > 0)
		                	testcases.write(driver.findElement(By.cssSelector("div.jd-info ul:nth-of-type(3)")).getText());
		                else
		                	testcases.write("-");
		                if( driver.findElements(By.cssSelector("div.jd-info ul:nth-of-type(4)")).size() > 0)
		                	testcases.write(driver.findElement(By.cssSelector("div.jd-info ul:nth-of-type(4)")).getText());
		                else
		                	testcases.write("-");
		                if( driver.findElements(By.cssSelector("div.jd-info ul:nth-of-type(5)")).size() > 0)
		                	testcases.write(driver.findElement(By.cssSelector("div.jd-info ul:nth-of-type(5)")).getText());
		                else
		                	testcases.write("-");
		                
		                testcases.endRecord();
		                //close the file
		                testcases.close();
		                if(i==10) {
		                	i=1;
				        	f=f+10;
		                }

		            } catch (IOException e) {
		                e.printStackTrace();
		            }
		            
		            
			}
		}

	}
