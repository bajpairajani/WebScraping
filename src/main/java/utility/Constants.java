package utility;

public class Constants {
	
	public static final String URL = "https://careers.accor.com/global/en/search-results";
		
	public static final String CHROMEDRIVER 	= "/Users/janetrajani/eclipse-workspace/chromedriver";
	public static final String FIREFOXDRIVER 	= "C:/Selenium/gecko.exe";
	public static final String EXPORTTOCSV 	= "accordJob0.csv";
}
